&GLOBAL
  PROJECT butadiene
  RUN_TYPE ENERGY
  PRINT_LEVEL LOW
&END GLOBAL

&FORCE_EVAL
  METHOD Quickstep              ! Electronic structure method
  &DFT
  BASIS_SET_FILE_NAME EMSL_BASIS_SETS
    &PRINT
      &MO_CUBES                 ! MOs to be written to cube-files.
        NHOMO 5
        NLUMO 5
      &END MO_CUBES
    &END PRINT
    &POISSON             
      PERIODIC NONE
      PSOLVER  WAVELET   
    &END POISSON
    &QS                         ! Parameters needed to set up the Quickstep framework
      METHOD GAPW               ! Method: PM6 semiempirical method 
    &END QS

    &SCF                        ! Convergence criteria for self consistent problem      
      EPS_SCF 1.0E-6
      SCF_GUESS ATOMIC
      MAX_SCF 150
      &OUTER_SCF
        EPS_SCF 1.0E-6
        MAX_SCF 100
      &END
    &END SCF

    &XC                        ! Parameters for exchange potential 
      &XC_FUNCTIONAL NONE   
      &END XC_FUNCTIONAL
      &HF                      ! Hartree Fock exchange.   
        &SCREENING             ! Setting screening of the electronic repulsion              
        &END SCREENING
      &END HF
    &END XC
  &END DFT

 &SUBSYS
    &CELL
      ABC 20 20 20
      PERIODIC NONE              ! Non periodic calculations. That's why the POISSON scetion is needed 
    &END CELL
    &TOPOLOGY                    ! Section used to center the atomic coordinates in the given box. Useful for big molecules
      &CENTER_COORDINATES
      &END
    &END
    &COORD
  C         3.2823510238        5.6246106912        4.6471368035
  C         4.5899199606        5.6014419456        4.9868092570
  C         5.4100751682        4.3985069429        5.0128498644
  C         6.7176033241        4.3753650795        5.3526833842
  H         2.7494733940        4.7089940776        4.3631265491
  H         2.7034227548        6.5533993353        4.6413242205
  H         5.0918300047        6.5392279953        5.2662184098
  H         4.9082070296        3.4607387529        4.7333057662
  H         7.2965617076        3.4465954580        5.3584928846
  H         7.2503949431        5.2909783872        5.6368682850
    &END COORD
    &KIND H                    ! Basis set and potential for H
     BASIS_SET aug-cc-pVDZ
     POTENTIAL ALL
    &END KIND
    &KIND C                    ! Basis set and potential for C
     BASIS_SET aug-cc-pVDZ
     POTENTIAL ALL
    &END KIND
  &END SUBSYS
&END FORCE_EVAL
