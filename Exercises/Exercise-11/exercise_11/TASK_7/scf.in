&CONTROL
    calculation = 'scf'
    restart_mode = 'from_scratch'    ! 'restart' needs a cleanly stopped calculation in 'outdir'
    wf_collect = .true.              ! store wfns in 'outdir'/'prefix'.save
    outdir = './'                    ! in outdir folder 'prefix'.save is created, where all calc results are saved
    prefix = '7agnr_2uc'             ! prepended to input/output filenames
    pseudo_dir = '../pseudo'          ! folder needs to include all files referenced in section ATOMIC_SPECIES
/
&SYSTEM
    ibrav = 8                        ! Simple lattice type with vectors (a, 0, 0), (0, b, 0), (0, 0, c)
    a = 8.59975208056
    b = 20
    c = 15
    nat = 36                         ! number of atoms in the unit cell
    ntyp = 2                         ! number of types of atoms in the unit cell
    ecutwfc = 30.0                  ! kinetic energy cutoff (Ry) for wavefunctions 
    ecutrho = 240.0                  ! ecut for charge density (norm-conserving 4*ecutwfc, for ultrasoft 8*ecutwfc) (corresponds to CP2K CUTOFF and determines the real accuracy)
    occupations = 'smearing'            ! 'smearing' for metals, 'fixed' for bad-gap materials
    degauss=0.001
/
&ELECTRONS
    electron_maxstep = 50            ! maximum number of iterations in a scf step (default: 100)
    conv_thr = 1.0e-8                ! SCF calculation convergence threshold (estimated energy error < conv_thr)
    mixing_beta = 0.25               ! mixing factor for SCF (default 0.7)
/
ATOMIC_SPECIES
C 12.01070 C_pbe_v1.2.uspp.F.UPF             ! Mass and pseudopotential for each element (uspp = ultrasoft pp, hgh=gth=geodecker-teter-hutter)
H 1.007940 H.pbe-rrkjus_psl.0.1.UPF
ATOMIC_POSITIONS angstrom
C       0.75378683       0.95835735       7.50000000
C       2.12425027       0.95835735       7.50000000
C       0.00933900       2.16406303       7.50000000
C       0.72126252       3.39344827       7.50000000
C       2.15677458       3.39344827       7.50000000
C       2.86869810       2.16406303       7.50000000
C       2.87803710       4.62982323       7.50000000
C       2.15677458       5.86619819       7.50000000
C       0.72126252       5.86619819       7.50000000
C      -0.00000000       4.62982323       7.50000000
C       0.00933900       7.09558343       7.50000000
C       0.75378683       8.30128911       7.50000000
C       2.12425027       8.30128911       7.50000000
C       2.86869810       7.09558343       7.50000000
H       0.23968726       9.25964646       7.50000000
H       2.63834984       9.25964646       7.50000000
H       0.23968726       0.00000000       7.50000000
H       2.63834984       0.00000000       7.50000000
C       5.05366287       0.95835735       7.50000000
C       6.42412631       0.95835735       7.50000000
C       4.30921504       2.16406303       7.50000000
C       5.02113856       3.39344827       7.50000000
C       6.45665062       3.39344827       7.50000000
C       7.16857414       2.16406303       7.50000000
C       7.17791314       4.62982323       7.50000000
C       6.45665062       5.86619819       7.50000000
C       5.02113856       5.86619819       7.50000000
C       4.29987604       4.62982323       7.50000000
C       4.30921504       7.09558343       7.50000000
C       5.05366287       8.30128911       7.50000000
C       6.42412631       8.30128911       7.50000000
C       7.16857414       7.09558343       7.50000000
H       4.53956330       9.25964646       7.50000000
H       6.93822588       9.25964646       7.50000000
H       4.53956330       0.00000000       7.50000000
H       6.93822588       0.00000000       7.50000000
K_POINTS automatic
10 1 1 0 0 0                        ! k-points in (x, y, z) directions and shift in (x, y, z)
