
#remove the file if it exist
if [ -f trajectory2.xyz ]
then
   rm trajectory2.xyz
fi

#create trajectory taking the final position of aech replica
for i in `seq 1 8`
   do tail -n 40 output_neb2-pos-Replica_nr_${i}-1.xyz >> trajectory2.xyz
done

#create energy file
grep E trajectory2.xyz | awk '{print NR, $6}' > energy2.dat
